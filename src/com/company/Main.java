package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Task2dot1();
        Task2dot2();
    }
    public static void Task2dot1() {
        //2. Дана программа для конвертации валют по введенному курсу (JavaExampleI.java).
        //2.1. На ее основе необходимо сделать программу для конвертации гривны в доллар по одному
        //из заранее известных (значения задаются в переменных/константах/моделях) курсов (например,
        //курс ПриватБанка, ОщадБанка, и банка ПУМБ). Название банка вводится с консоли.

        System.out.println();
        System.out.println("Task 2.1");

        //init variables

        String USD = "USD";

        Banks[] bank = new Banks[3];
        bank[0].usdCourse = 24.5f;
        bank[0].bankName = "privatbank";

        bank[1].usdCourse = 30.5f;
        bank[1].bankName = "pumb";

        bank[2].usdCourse = 44.5f;
        bank[2].bankName = "oschadbank";

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change to USD: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter bank
        System.out.println("Enter from which bank do you want to convert your grivnas (you can use only PUMB or PRIVATBANK or OSHADBANK): ");
        String bankName = scan.nextLine();

        for (int i = 0; i < bank.length; i++) {
            if (bankName.equals(bank[i].bankName)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / bank[i].usdCourse));
                break;
            } else if (i == bank.length - 1) {
                System.out.println("Smth went wrong");
            }
        }
    }

    public static void Task2dot2() {
//        //2. Дана программа для конвертации валют по введенному курсу (JavaExampleI.java).
//        //2.1. На ее основе необходимо сделать программу для конвертации гривны в доллар по одному
//        //из заранее известных (значения задаются в переменных/константах/моделях) курсов
//        //(например, курс ПриватБанка, ОщадБанка, и банка ПУМБ). Название банка вводится с консоли.
//        System.out.println();
//        System.out.println("Task 2.2");
//
//        //init variables
//
//        String USD = "USD";
//        String EUR = "EUR";
//        String RUB = "RUB";
//
//        Courses privat = new Courses();
//        privat.eurCourse = 30.01f;
//        privat.rubCourse = 0.41f;
//        privat.usdCourse = 24.5f;
//        privat.bankName = "privatbank";
//
//        Courses pumb = new Courses();
//        pumb.rubCourse = 0.42f;
//        pumb.eurCourse = 31f;
//        pumb.usdCourse = 27.5f;
//        pumb.bankName = "pumb";
//
//        Courses oschad = new Courses();
//        oschad.usdCourse = 26.5f;
//        oschad.eurCourse = 30f;
//        oschad.rubCourse = 0.43f;
//        oschad.bankName = "oschadbank";
//
//        //init scanner
//        Scanner scan = new Scanner(System.in);
//
//        //enter amount of money
//        System.out.println("Enter the amount of money that you want to change to USD: ");
//        int amount = Integer.parseInt(scan.nextLine());
//
//        //enter bank
//        System.out.println("Enter from which bank do you want to convert your grivnas (you can use only PUMB or PRIVATBANK or OSHADBANK): ");
//        String bank = scan.nextLine();
//
//        //enter currency
//        System.out.println("Enter to what kind of currency do you want to change your grivnas (you can use only EUR or USD or RUB): ");
//        String currency = scan.nextLine();
//
//        //convert uah to preferred currency
//        for (int i = 0; i < 3; i++) {
//
//        }
//        if (oschad.bankName.equalsIgnoreCase(bank)) {
//            if (currency.equalsIgnoreCase(EUR)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / oschad.eurCourse));
//            } else if (currency.equalsIgnoreCase(USD)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / oschad.usdCourse));
//            } else if (currency.equalsIgnoreCase(RUB)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / oschad.rubCourse));
//            } else {
//                System.err.println("Sorry for inconvenience. Please try again later :)");
//            }
//        } else if (privat.bankName.equalsIgnoreCase(bank)) {
//            if (currency.equalsIgnoreCase(EUR)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / privat.eurCourse));
//            } else if (currency.equalsIgnoreCase(USD)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / privat.usdCourse));
//            } else if (currency.equalsIgnoreCase(RUB)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / privat.rubCourse));
//            } else {
//                System.err.println("Sorry for inconvenience. Please try again later :)");
//            }
//        } else if (pumb.bankName.equalsIgnoreCase(bank)) {
//            if (currency.equalsIgnoreCase(EUR)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / pumb.eurCourse));
//            } else if (currency.equalsIgnoreCase(USD)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / pumb.usdCourse));
//            } else if (currency.equalsIgnoreCase(RUB)) {
//                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / pumb.rubCourse));
//            } else {
//                System.err.println("Sorry for inconvenience. Please try again later :)");
//            }
//        } else {
//            System.err.println("You did smth wrong, please do not use our system anymore, you are poor client :)");
//        }
    }
}
